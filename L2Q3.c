#include "resposta.h"

int main(void) {
    ABB*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    raiz = withdraw_odd(raiz); //Retorna a árvore sem os números impares
    printf("A arvore sem os numeros impares:\n");
    call_print_tree(raiz);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}