#ifndef RESPOSTA_H
#define RESPOSTA_H

#include "busca.h"

ABB * bigger(ABB * r); // QUESTÃO 1
ABB * smaller(ABB * r); //QUESTÃO 2
ABB * withdraw_odd(ABB * r); //QUESTÃO 3
ABB * free_node(ABB * r); // QUESTÃO 3
// QUESTÃO 4 ESTÁ RESPONDIDA NO ARQUIVO "stacktree.h"

ABB * bigger(ABB * r) {
    if(r->dir) return bigger(r->dir); // Enquanto existir elemento à direita
    // chama a função em recursão à direita.
    return r; // Quando encontrar a folha da extrema direita, a retorna
    //Esse nó folha quando é encontrado, é o valor de retorno em todas as recursões.
    //A folha é retornada para o pai, e do pai para o seu respectivo pai, até a 
    // chamada original da função
}

ABB * smaller(ABB * r) {
    if(r->esq) return smaller(r->esq); // Enquanto existir elemento à esquerda
    // chama a função em recursão à esquerda.
    return r; // Quando encontrar a folha da extrema esquerda, a retorna
    //Esse nó folha quando é encontrado, é o valor de retorno em todas as recursões.
    //A folha é retornada para o pai, e do pai para o seu respectivo pai, até a 
    // chamada original da função
}

ABB * withdraw_odd(ABB * r) {
    /* Como nos casos de liberação, é necessário o percurso pós ordem para não
    perder nenhum nó */
    if(r->esq)r->esq = withdraw_odd(r->esq);//Garante que percorrerá um nó existente
    if(r->dir)r->dir = withdraw_odd(r->dir);//Garante que percorrerá um nó existente
    // Percorre recursivamente até as folhas e nos retornos opera a remoção
    if(r->info % 2 == 1) return free_node(r);//Se for impar, remove o nó, e apenas ele
    return r; // Retorna o nó para o pai, ou para a chamda da função(caso raiz)
}

ABB * free_node(ABB * r) {
    if(r->esq == NULL && r->dir == NULL) { // Caso de nó folha
        free(r); // Libera o nó
        return NULL; // Retorna NULL para o ponteiro pai
    }
    if((r->esq&&!r->dir)||(!r->esq&&r->dir)) { // Caso filho úncio
        free(r); // Libera o nó
        if(r->esq) return r->esq; // Retorna filho único do nó removido para o pai
        else return r->dir; // para ocupar a mesma posição do nó removido
    // Casos em que há dois filhos
    }if(r->dir->esq) { /* Caso em que existe filho da esquerda, no filho da direita.
    É usado essa abordagem tendo em vista o uso de árvores binárias.
    O filho da esquerda é a subárvore onde tem elementos menores, seu filho da extrema
    esquerda representa o menor elemento dessa subárvore, mantendo o balanceamento*/
	    ABB * p = r->dir->esq; //Inicializa nó para substituir
	    while(p->esq) p=p->esq;//Enquanto houver filho à esquerda, há elementos menores
        r->info =p->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        /* Abaixo há a chamda recursiva, onde o menor elemento da subárvore direita
        pode ter no máximo um filho, assim essa próxima chamada recursiva é a última*/
        p = free_node(p); // Remove o nó redundante
    }
    else { // Caso em que o filho da esquerda não tem filho à direita
    /*Nesse caso o filho da direita substitui o pai, e é chamda a função para removê-lo
    Tendo em vista que ele não tem um filho na direita, senão teria entrado no caso
    acima, Na próxima chamada ele entrará no segundo caso(filho único) e o problema
    será resolvido*/
        r->info = r->dir->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        r->dir = free_node(r->dir); // Remove o nó redundante
    }
}

#endif //RESPOSTA_H
