#include "resposta.h"

int main(void) {
    ABB*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    ABB * n = smaller(raiz); //Retorna o nó com o menor elemento da árvore
    printf("O menor elemento da arvore e:  %d\n", n->info);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}