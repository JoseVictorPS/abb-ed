#include "stacktree.h"

int main(void) {
    ABB*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    int n = read_int(); //Valor que será usado na operação

    int * v = less_than(raiz, n);//Retorna vetor com números menores que n, da árvore
    //free(v); //Libera o vetor v

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}