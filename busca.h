#ifndef BUSCA_H
#define BUSCA_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct arv { // Nó de cada árvore
    int info; // Informação de cada nó
    struct arv * esq; // Ponteiro para o filho a esquerda
    struct arv * dir; // Ponteiro para o filho a direita
}ABB;

ABB * initialize();
ABB * gen_leaf(int c);
int empty_tree(ABB * a);
ABB * free_tree(ABB * a);
ABB* build_tree(ABB * r);
ABB * insert_tree(ABB * r, int v);
void print_tree(ABB*r);
void call_print_tree(ABB*r);
int read_value();
ABB*UX_tree();
ABB * random_tree();


ABB * initialize() {return NULL;}// Retorna valor nulo para o ponteiro

ABB * gen_leaf(int c) {
    ABB * p = (ABB*)malloc(sizeof(ABB)); // Aloca nó de árvore
    p->info = c; //Preenche campo info do nó
    p->dir = NULL; // Filho da direita de folha é sempre vazio
    p->esq = NULL; // Filho da esquerda de folha é sempre vazio

    return p; // Retorna o nó
}

int empty_tree(ABB * a) {return a==NULL;}// Retorna se árvore está vazia

ABB * free_tree(ABB * a) { //É preciso ir até os filhos e depois liberar o nó
    if(!empty_tree(a)) { // Se árvore não está vazia
        free_tree(a->esq); // Vai até o filho da esquerda
        free_tree(a->dir); // Vai até o filho da direita
        free(a); // Libera o nó em questão
    }
    return NULL; // Atualiza o valor da árvore para nulo(vazia)
}



void print_tree(ABB*r) {
    printf("<");//Sinal de "menor que" significa abertura uma árvore/subárvore
    if(!empty_tree(r)) { // Se não estiver vazia
        printf(" %d ", r->info); // Imprime conteúdo do nó
        print_tree(r->esq); //Chama a função para a subárvore da esquerda
        print_tree(r->dir); //Chama a função para a subárvore da direita
    }
    printf("> "); // Fecha o conteúdo da árvore/subárvore
}

void call_print_tree(ABB*r) { print_tree(r); printf("\n"); }

ABB* build_tree(ABB * r) {
    int v = read_value(); // Lê um valor para inserção
    if(empty_tree(r)) { // Se a árvore/subárvore estiver vazia
        r = gen_leaf(v);//Cria uma folha
        return r;
    }
    return insert_tree(r, v); // Insere nos padrões da árvore de busca binária
    //PS: O elemento inserido sempre será inserido como folha. Ele será inserido
    //na subárvore da esquerda, caso seu campo info seja menor do que o da raíz atual
    //e na subárvore da direita, caso seja maior
}
ABB * insert_tree(ABB * r, int v) {
    if(empty_tree(r)) { // Se a árvore/subárvore estiver vazia
        r = gen_leaf(v);//Cria uma folha
        return r;
    }
    // Sempre retorna o nó filho chamado ao pai, durante a recursão, os nós já
    // existentes não são alterados, logo essa operação não altera a árvore
    // apenas quando se insere a folha, aí altera o pai da respectiva folha
    if(v > r->info) r->dir = insert_tree(r->dir, v);
    if(v < r->info) r->esq = insert_tree(r->esq, v);
    return r; // Retorna o nó da chamada atual da recursão para o pai
}

int read_value() {
    printf("Qual o valor do nó que deseja inserir?  ");
    int v; // Inicializa valor a inserir no nó
    scanf("%d",&v); // Escaneia valor
    return v;
}

ABB*UX_tree() {
    int op; // Inicializa variável de interação com usuário
    ABB*raiz = initialize(); // Inicializa árvore binária
    
    do{ // Laços de preenchimento da árvore com interação com usuário
    raiz = build_tree(raiz); // Contrução da árvore nó a nó
    call_print_tree(raiz); // Imprime a árvore no momento atual

    printf("Deseja operar novamente?  ");
    scanf("%d", &op); // Escaneia se há mais nós para inserir
    }while(op);

    return raiz; // Retorna a árvore criada
}

ABB * random_tree() {
    ABB*raiz = initialize();//Inicializa árvore binária

    srand(time(NULL)); //Para prevenir repetição de sequencias entre execuções
    for(int i=0; i<12; i++)raiz=insert_tree(raiz, 1 + rand()%30);//Entre 1 e 30 inclusos

    call_print_tree(raiz);
    return raiz;
}

#endif //BUSCA_H