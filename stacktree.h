#ifndef STACKTREE_H
#define STACKTREE_H

#include "busca.h"
#include "gen_stack.h"

int read_int(); //QUESTÃO 4
int * less_than(ABB * r, int n); //QUESTÃO 4
void print_stk(STK * p); //QUESTÃO 4
void print_cb(void * p);//QUESTÃO 4
void make_stack(STK * p, ABB * r, int n); //QUESTÃO 4
int size_stack(STK * p); //QUESTÃO 4
void size_stack_cb(void * p, void * i);//QUESTÃO 4
int * make_array(STK * p, int tam); //QUESTÃO 4
void make_array_cb(void * p, void * q, void * pos);//QUESTÃO 4

int read_int() {
    printf("Qual o valor que deseja operar?  ");
    int v; // Inicializa valor
    scanf("%d",&v); // Escaneia valor
    return v;
}

int * less_than(ABB * r, int n) {
    int * resp; // Inicializa ponteiro resposta
    STK * p = create_s(); //Inicializa pilha
    make_stack(p, r, n); //Preenche a pilha com os elementos menores que n da árvore r
    int tam = size_stack(p); //Recebe o tamanho da pilha
    resp = make_array(p, tam); //Gera um vetor baseado nos elementos e tamanho da pilha
    print_stk(p); // Imprime a pilha
    free_s(p); // Libera a pilha
    return resp;
}

void print_stk(STK * p) {
    if(empty_s(p)) return; //Se pilha estiver vazia, não faz nada
    printf("O vetor e:");
    void (*callback) (void*); //Inicializa callback(ponteiro para função)
    callback = print_cb; //Faz callback apontar para função print_cb
    run_stk(p, callback); //Chama função para percorrer a pilha chamando callback
    printf("\n");
}

void make_stack(STK * p, ABB * r, int n) {
    if(r->info < n) push(p, r);//Insere na pilha o nó com info menor que n
    if(r->esq)make_stack(p, r->esq, n);//Chama recursão para subárvore esquerda
    if(r->dir)make_stack(p, r->dir, n);//Chama recursão para subárvore direita
}

int size_stack(STK * p) {
    int count = 0;//Inicia contador
    void (*callback) (void*, void*); //Inicializa callback(ponteiro para função)
    callback = size_stack_cb; //Callback aponta para função auxiliar size_stack_cb
    run_stk_2(p, callback, &count);//Chama função p percorrer a pilha
    return count; //Retorna contador
}

void size_stack_cb(void * p, void * i) {
    int*j = (int*) i;//Typecast de ponteiro para acessar a dereferenciação
    (*j)++; //Incrementa count pela dereferenciação do ponteiro j
}

int * make_array(STK * p, int tam) {
    if(empty_s(p)) return NULL; //Se pilha estiver vazia, retorna nulo
    int * r = (int *)malloc(sizeof(int) * tam);//Aloca vetor de inteiro de tamanho tam
    void (*callback)(void*, void*, void*);//Inicia callback para manipular dois dados
    callback = make_array_cb;//Faz ponteiro de função apontar para make_array_cb
    int pos = 0;//Inicia posição para iterar no vetor
    run_stk_3(p, callback, r, &pos);//Percorre a pilha
    return r; //Retorna o vetor
}

void print_cb(void * p) {
    ABB * q = (ABB *) p; //Typecast de ponteiro void para ABB* para acessar os campos
    printf(" %d", q->info);
}

void make_array_cb(void * p, void * q, void * pos) {
    ABB * a = (ABB*)p; //Typecast para acessar campos do nó da árvore
    int * v = (int*)q; //Typecast para acessar posição do vetor
    int * j = (int *) pos;//Typecast para acessar dereferenciação em variável
    v[(*j)] = a->info;//Preenche vetor com auxílio de deferenciação
    (*j)++; //Atualiza variável variável pos através de deferenciação
}

#endif // STACKTREE_H