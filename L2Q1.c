#include "resposta.h"

int main(void) {
    ABB*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    ABB * n = bigger(raiz); //Retorna o nó com o maior elemento da árvore
    printf("O maior elemento da arvore e:  %d\n", n->info);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}