#ifndef GENSTACK_H
#define GENSTACK_H

#include <stdlib.h>
#include <stdio.h>

typedef struct lista { // Estrutura de pilha genérica(lista dinamica)
    void * node;// Cada nó guarda um ponteiro genérico
    struct lista * prox; // Ponteiro para o próximo da pilha
}List;

typedef struct stack { //Descritor da pilha
    List * ini; // Ponteiro para o início da pilha
}STK;

STK * create_s (void);
void push( STK * f, void * node);
void * pop(STK * f);
int empty_s(STK * f);
void free_s(STK * f);
void run_stk(STK * p, void (*cb)(void*));
void run_stk_2(STK * p, void (*cb)(void*, void*), void * dado);
void run_stk_3(STK * p, void (*cb)(void*, void*, void*), void * dado, void * valor);
void remove_stk(STK*f, void*p);
List * find_stk(STK*f, void*p);


STK * create_s (void) {
    STK * f = (STK *) malloc(sizeof(STK)); // Aloca espaço para a pilha
    f->ini = NULL; // Ponteiro de início são anulados
    return f; // Retorna a pilha
}

void push( STK * f, void * node) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->node = node; // Insere endereço para o campo node genérico
    n->prox = f->ini; // Ponteiro para o próximo é o antigo primeiro da pilha
    f->ini = n; // O começo apontará para n
}

void * pop(STK * f) {
    List * t; // Inicia ponteiro auxiliar
    void * node; // Inicia valor para retornar
    if(empty_s(f)) {
        printf("Fila vazia!\n");
        return NULL; // Retorna para onde a função foi chamada
    }
    t = f->ini; // Ponteiro t aponta para o primeiro nó
    node = t->node; // Valor recebe o que está no campo node do nó t
    f->ini = t->prox; // Primeiro da fila passa a ser o segundo nó da fila
    free(t); // Libera o nó de t, que ocupava a primeira posição na fila
    return node; // Retorna valor que estava no nó
}

int empty_s(STK * f) {
    return (f->ini == NULL); // Retorna se o começo da fila existe ou não
}

void free_s(STK * f) {
    if(f->ini == NULL){
        free(f); // Libera a estrutura fila, com os campos ini e fim
        return;
    }
    List * q = f->ini; // Cria um nó para iteração, apontando para o começo da fila
    List * t; // Cria um ponteiro auxiliar
    while(q) {
        t = q->prox; // Ponteiro auxiliar recebe o próximo de q
        free(q); // Libera q
        q = t; // Atualiza q com o valor de seu próximo, que estava salvo em t
    }
    free(f); // Libera a fila
}

/*Abaixo estão as funções que percorrem a pilha com auxílio da callback para operar
os nós. Note que por usarmos ponteiros genéricos, não temos acesso aos campos dos nós
para o qual ->node aponta, para isso precisamos de uma função(callback) que usa typecast
no endereço armazenado no nó para operar. Callback é um ponteiro para função, que deve
ser inicializado com a mesma assinatura da função a qual deseja representar*/

void run_stk(STK * p, void (*cb)(void*)) { //Função para percorrer com uso de callback
    List * q; // Inicia variável de iteração
    for(q=p->ini; q; q=q->prox) cb(q->node); //A cada iteração, chama o callback
}

void run_stk_2(STK * p, void (*cb)(void*, void*), void * dado) {
    //Caso precise manipular um dado adicional, como ponteiro, struct, etc
    List * q; // Inicia variável de iteração
    for(q=p->ini; q; q=q->prox) cb(q->node, dado); //A cada iteração, chama o callback
}

void run_stk_3(STK * p, void (*cb)(void*, void*, void*), void * dado, void * valor) {
    //Caso precise manipular um outro dado adicional, como posição do vetor, etc
    List * q; // Inicia variável de iteração
    for(q=p->ini; q; q=q->prox) cb(q->node, dado, valor);//A cada iteração,chama o callback
}

void remove_stk(STK*f, void*p) {
    List*l = f->ini, *ant=NULL;//Inicia ponteiro auxiliar e anterior
    //Enquanto não tiver encontrado, atualiza o auxiliar e o anterior
    while((l) && (l->node != p)) { ant=l; l=l->prox; }
    if(!l) return;//Se não encontrou, retorna a lista inalterada
    //Se o nó é o primeiro, atualiza o campo início da pilha
    if(l == f->ini) f->ini = l->prox;
    //Amarra o próximo do anterior com o próximo do nó a ser removido
    if(ant) ant->prox = l->prox;
    free(l);//Libera o nó desejado
    return;
}

List * find_stk(STK*f, void*p) {
    List*l=f->ini; //Inicia ponteiro para iteração
    //Enquanto o ponteiro estiver dentro da pilha e o campo node,
    //for diferente do nó que buscamos, atualiza para o próximo
    while((l) && (l->node!=p)) l=l->prox;
    //Se encontrar, retorna um ponteiro válido, caso não tenha encontrado
    //retorna NULL
    return l;
}

#endif //GENSTACK_H

